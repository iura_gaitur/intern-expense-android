package com.assist.iuriegaitur.expensemanager.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.adapters.MainPagerAdapter
import com.assist.iuriegaitur.expensemanager.models.Category
import com.assist.iuriegaitur.expensemanager.models.Transaction
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.TransactionRepository
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.DatePickerUtil
import com.assist.iuriegaitur.expensemanager.utils.DateUtil
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import com.assist.iuriegaitur.expensemanager.utils.TimePickerUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_action.*
import kotlinx.coroutines.*
import java.lang.Double.parseDouble
import java.lang.Exception
import java.util.*

class AddActionActivity : AppCompatActivity(), DatePickerUtil.AlertDialogInterface,
        TimePickerUtil.TimeAlertDialogInterface, MainPagerAdapter.OnUserAction {
    private var categoryAdaper: MainPagerAdapter? = null
    private var selectedDate: String? = null
    private var selectedTime: String? = null
    private var amountTransaction: String? = null
    private var nameTransaction: String? = null
    private var categoryList = mutableListOf<Category>()
    private var selectedImage: Uri? = null
    private var FilePathUri: Uri? = null
    private var Image_Request_Code = 7
    private var dateTime = Calendar.getInstance()
    private var savedDateTimeMilis: Long = 0
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_action)

        getCategories()
        showCategoryAdapter()
        initDateAndTime()
        initActionListeners()
    }

    private fun getCategories() {
        val imageCategory = intArrayOf(R.drawable.ic_hand, R.drawable.ic_groceries, R.drawable.ic_car,
                R.drawable.ic_laundry, R.drawable.ic_savings, R.drawable.ic_doctor, R.drawable.ic_beauty,
                R.drawable.ic_travel)
        val nameCategory = intArrayOf(R.string.income, R.string.food, R.string.car,
                R.string.clothes, R.string.savings, R.string.health, R.string.beauty, R.string.travel)

        for (index in 0 until 8) {
            categoryList.add(Category(index, imageCategory[index], getString(nameCategory[index])))
        }
    }

    private fun showCategoryAdapter() {
        val layoutGrid = GridLayoutManager(parent, 4)
        categoryRecyclerView.setHasFixedSize(true)
        categoryRecyclerView.layoutManager = layoutGrid
        categoryAdaper = MainPagerAdapter(categoryList, this)
        categoryRecyclerView.adapter = categoryAdaper
    }

    private fun initDateAndTime() {
        val date = DateUtil().getCurrentDate()
        val time = DateUtil().getCurrentHour()
        savedDateTimeMilis = dateTime.timeInMillis
        actionDate.text = "$date " + "at" + " $time"
    }

    private fun initActionListeners() {
        camera.setOnClickListener {
            startCamera()
        }

        actionDate.setOnClickListener {
            openDateTimePickerDialog()
        }

        saveTransaction.setOnClickListener {
            saveTransaction()
        }

        arrowBack.setOnClickListener { finish() }
    }

    private fun startCamera() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), Image_Request_Code)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Image_Request_Code &&
                resultCode == Activity.RESULT_OK &&
                data != null && data.data != null) {

            FilePathUri = data.data
            CropImage.activity(FilePathUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this)
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                selectedImage = result.uri
                showSelectedImage()
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, result.error.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        if (requestCode == RESPONSE_CODE) {
            val returnIntent = Intent()
            returnIntent.putExtra(GET_RESULT, RESULT)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    private fun showSelectedImage() {
        Glide.with(applicationContext).load(selectedImage).apply(RequestOptions().circleCrop()).into(camera)
    }

    private fun openDateTimePickerDialog() {
        val datePickerFragment = DatePickerUtil(this)
        val timePickerFragment = TimePickerUtil(this)

        datePickerFragment.show(supportFragmentManager, "Date picker")
        timePickerFragment.showPicker(View(this))
    }

    private fun saveTransaction() {
        if (!validateFields()) return

        try {
            GlobalScope.launch {
                getCurrentUser()
                saveTransactionRepository()
            }
            showMessage(getString(R.string.transaction_saved))
            val returnIntent = Intent()
            returnIntent.putExtra(GET_RESULT, true)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        } catch (ex: Exception) {
            showMessage(getString(R.string.something_went_wrong_transaction))
        }
    }

    private fun validateFields(): Boolean {
        amountTransaction = amountField.text.toString()
        val emptyAmountTransactionField = amountTransaction.isNullOrEmpty()
        val emptyNameTransactionField = nameTransaction.isNullOrEmpty()

        return if (!emptyAmountTransactionField && !emptyNameTransactionField) {
            true
        } else {
            showMessage(getString(R.string.introduce_amount_category))
            return false
        }
    }

    private suspend fun getCurrentUser() {
        user = GlobalScope.async(Dispatchers.IO) {
            SharedPrefUtil(applicationContext).getEmail()?.let {
                UserRepository(applicationContext).getPrimaryUser(it)
            }
        }.await()!!
    }

    private suspend fun saveTransactionRepository() {
        GlobalScope.async(Dispatchers.IO) {
            delay(2000)
            getTransaction()?.let { TransactionRepository(this@AddActionActivity).saveTransaction(it) }
        }.await()
    }

    private fun getTransaction(): Transaction {
        user?.let {
            return Transaction(
                    id = System.currentTimeMillis(),
                    userId = it.id,
                    name = nameTransaction,
                    amount = parseDouble(amountTransaction),
                    dateTime = savedDateTimeMilis,
                    details = amountDetails.text.toString(),
                    photo = selectedImage.toString())
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(this@AddActionActivity, message, Toast.LENGTH_SHORT).show()
    }

    override fun onUserClickShowCategory(category: Category) {
        nameTransaction = category.title
        categoryAdaper?.stateCategory(category.id)
    }

    override fun timeInputWithoutFormat(hour: Int, minute: Int, seconds: Int) {}

    override fun setDate(date: String) {
        selectedDate = date
    }

    override fun setDateTime(date: Long) {
        savedDateTimeMilis = date
    }

    override fun timeInput(time: String) {
        selectedTime = time
        actionDate.text = selectedDate + " " + "at" + " " + selectedTime
    }

    companion object {
        fun show(activity: Activity) {
            activity.startActivity(Intent(activity, AddActionActivity::class.java))
        }

        const val RESPONSE_CODE = 111
        const val RESULT = "result"
        const val GET_RESULT = "intent get extra result"
    }
}



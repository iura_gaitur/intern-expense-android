package com.assist.iuriegaitur.expensemanager.utils

import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import java.util.ArrayList

object ChartHelper {
    var entries: MutableList<Entry> = mutableListOf()
    var entriesBar = ArrayList<BarEntry>()

    fun clearEntries() {
        entries.clear()
        entriesBar.clear()
    }
}
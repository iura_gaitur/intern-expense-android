package com.assist.iuriegaitur.expensemanager.network

data class ResponseCurrency(
        var base: String,
        var date: String,
        var rates: Rates
)

data class Rates(
        var AED: Double,
        var AUD: Double,
        var BGN: Double,
        var BRL: Double,
        var CAD: Double,
        var CHF: Double,
        var CNY: Double,
        var CZK: Double,
        var DKK: Double,
        var EGP: Double,
        var EUR: Double,
        var GBP: Double,
        var HRK: Double,
        var HUF: Double,
        var INR: Double,
        var JPY: Double,
        var KRW: Double,
        var MDL: Double,
        var MXN: Double,
        var NOK: Double,
        var NZD: Double,
        var PLN: Double,
        var RSD: Double,
        var RUB: Double,
        var SEK: Double,
        var THB: Double,
        var TRY: Double,
        var UAH: Double,
        var USD: Double,
        var XAU: Double,
        var XDR: Double,
        var ZAR: Double
)

class CurrencyModel(
        var currencyName: String = "",
        var currencyRate: Double = 0.0
)

fun convertor(rates: Rates): MutableList<CurrencyModel> {
    val currencyList = mutableListOf<CurrencyModel>()
    currencyList.add(CurrencyModel("USD", rates.USD))
    currencyList.add(CurrencyModel("EUR", rates.EUR))
    currencyList.add(CurrencyModel("GBP", rates.GBP))
    currencyList.add(CurrencyModel("PLN", rates.PLN))
    currencyList.add(CurrencyModel("RUB", rates.RUB))
    return currencyList
}
package com.assist.iuriegaitur.expensemanager.repository

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.assist.iuriegaitur.expensemanager.models.Transaction

@Dao
interface TransactionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTransaction(transaction: Transaction)

    @Query("SELECT * FROM transactions WHERE userId=:userId")
    fun getTransaction(userId: Long): MutableList<Transaction>

    @Query("SELECT * FROM transactions WHERE name=:name GROUP BY dateTime")
    fun getSpecificTransaction(name: String): MutableList<Transaction>

    @Query("SELECT * FROM transactions WHERE  userId=:userId AND dateTime BETWEEN :startDate AND :endDate")
    fun getTransactionFromSpecificTime(userId: Long, startDate: Long, endDate: Long): MutableList<Transaction>
}
package com.assist.iuriegaitur.expensemanager.models

class Category(
        var id: Int = 0,
        var image: Int,
        var title: String,
        var categoryOnClick: Boolean = false
)
package com.assist.iuriegaitur.expensemanager.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Toast
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.models.Transaction
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.TransactionRepository
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.ChartHelper
import com.assist.iuriegaitur.expensemanager.utils.ChartUtil
import com.assist.iuriegaitur.expensemanager.utils.DateUtil
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import kotlinx.android.synthetic.main.fragment_budget.*
import kotlinx.coroutines.*
import java.lang.Exception

open class BudgetFragment : Fragment() {
    private var detailsView: View? = null
    private var weekTransactionList = mutableListOf<Transaction>()
    private var monthTransactionList = mutableListOf<Transaction>()
    private var transactionList = mutableListOf<Transaction>()
    private val barChart = ChartUtil()
    private var user: User? = null
    private var spaceLineChart=0.8f

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        detailsView = inflater.inflate(R.layout.fragment_budget, null)
        return detailsView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GlobalScope.launch { getTransactionsByTime() }
    }

    override fun onResume() {
        super.onResume()
        ChartHelper.clearEntries()
    }

    private suspend fun getTransactionsByTime() {
        GlobalScope.async(Dispatchers.IO) {
            try {
                getCurrentUser()
                getTransactionsByWeek()
                getTransactionsByMonth()
                getTransactionsByYear()
                setUserInfo()
            } catch (ex: Exception) {
                showMessage("Something went wrong")
            }
        }.await()
    }

    private suspend fun getCurrentUser() {
        user = GlobalScope.async(Dispatchers.IO) {
            delay(2000)
            SharedPrefUtil(context!!.applicationContext).getEmail()?.let {
                UserRepository(context!!.applicationContext).getPrimaryUser(it)
            }
        }.await()
    }

    private suspend fun getTransactionsByWeek() {
        GlobalScope.async(Dispatchers.IO) {
            val startWeekDate = DateUtil().getWeekStartDate()
            val endWeekDate = DateUtil().getWeekEndDate()
            user?.let {
                weekTransactionList = TransactionRepository(context!!.applicationContext).getTransactionByTime(it.id, startWeekDate, endWeekDate)
            }
        }.await()
    }

    private suspend fun getTransactionsByMonth() {
        GlobalScope.async(Dispatchers.IO) {
            val startMonthDate = DateUtil().getFirstDayOfMonth()
            val endMonthDate = DateUtil().getLastDayOfMonth()
            user?.let {
                monthTransactionList = TransactionRepository(context!!.applicationContext).getTransactionByTime(it.id, startMonthDate, endMonthDate)
            }
        }.await()
    }

    private suspend fun getTransactionsByYear() {
        GlobalScope.async(Dispatchers.IO) {
            val startMonthDate = DateUtil().getAllFirstMonthDayList()
            val endMonthDate = DateUtil().getAllLastMonthDayList().first
            var indexLine = 0f
            for (index in 0 until 12) {
                user?.let { user ->
                    TransactionRepository(context!!.applicationContext).getTransactionByTime(user.id, startMonthDate[index],
                            endMonthDate[index]).let { ChartUtil().addMonthData(it, indexLine) }
                }
                indexLine += spaceLineChart
            }
        }.await()

        GlobalScope.async(Dispatchers.Main) {
            barChart.createCombineChart(chart)
        }
    }

    private suspend fun setUserInfo() {
        GlobalScope.async(Dispatchers.Main) {
            getCurrentBalance()?.let { currentBalance.text = String.format("%.2f", it) }
            monthBalance.text = String.format("%.2f", getMonthIncomeAndBalance().second)
            monthIncome.text = String.format("%.2f", getMonthIncomeAndBalance().first)
            weekIncome.text = String.format("%.2f", getWeekIncome())
        }.await()
    }

    private fun getWeekIncome(): Double {
        var weekIncomeAmount = 0.0

        for (transactions in weekTransactionList) {
            if (transactions.name == "Income") {
                weekIncomeAmount += transactions.amount
            }
        }
        return weekIncomeAmount
    }

    private fun getMonthIncomeAndBalance(): Pair<Double, Double> {
        var monthIncomeAmount = 0.0
        var expenses = 0.0
        val currentMonthBalance: Double

        for (transactions in monthTransactionList) {
            if (transactions.name == "Income") {
                monthIncomeAmount += transactions.amount
            }
        }

        for (transactions in monthTransactionList) {
            if (transactions.name != "Income") {
                expenses += transactions.amount
            }
        }
        currentMonthBalance = monthIncomeAmount - expenses
        return Pair(monthIncomeAmount, currentMonthBalance)
    }

    private suspend fun getCurrentBalance(): Double? {
        var income = 0.0
        var expenses = 0.0

        GlobalScope.async(Dispatchers.IO) {
            user?.let {
                transactionList = TransactionRepository(context!!.applicationContext).getTransaction(it.id)
            }
        }.await()
        for (transactions in transactionList) {
            if (transactions.name == "Income") {
                income += transactions.amount
            }
        }

        for (transactions in transactionList) {
            if (transactions.name != "Income") {
                expenses += transactions.amount
            }
        }
        return income - expenses
    }

    private fun showMessage(message: String) {
        GlobalScope.async(Dispatchers.Main) { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }
}
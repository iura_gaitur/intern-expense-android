package com.assist.iuriegaitur.expensemanager.models

import android.arch.persistence.room.*

@Entity(tableName = "transactions")
class Transaction {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = 0

    @ColumnInfo(name = "userId")
    var userId: Long? = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "foreignAmount")
    var amount: Double = 0.0

    @ColumnInfo(name = "dateTime")
    var dateTime: Long? = null

    @ColumnInfo(name = "details")
    var details: String? = null

    @ColumnInfo(name = "photo")
    var photo: String? = null

    @Ignore
    constructor()

    constructor(id: Long? = null, userId: Long? = null, name: String? = null, amount: Double = 0.0, dateTime: Long? = null,
                details: String? = null, photo: String? = null) {
        this.id = id
        this.userId = userId
        this.name = name
        this.amount = amount
        this.dateTime = dateTime
        this.details = details
        this.photo = photo
    }
}

package com.assist.iuriegaitur.expensemanager.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.adapters.ExpensesAdapter
import com.assist.iuriegaitur.expensemanager.models.Transaction
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.TransactionRepository
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.ChartUtil
import com.assist.iuriegaitur.expensemanager.utils.DateUtil
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import kotlinx.android.synthetic.main.fragment_expenses.*
import kotlinx.coroutines.*
import java.lang.Exception

open class ExpensesFragment : Fragment() {
    private var detailsView: View? = null
    private var expensesAdaper: ExpensesAdapter? = null
    private var weekTransactionList = mutableListOf<Transaction>()
    private var monthTransactionList = mutableListOf<Transaction>()
    private var yearTransactionList = mutableListOf<Transaction>()
    private val pieData = ChartUtil()
    private var user: User? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        detailsView = inflater.inflate(R.layout.fragment_expenses, null)
        return detailsView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getTransactionsByTime()
        initButtonListener()
    }

    private fun initButtonListener() {
        GlobalScope.async(Dispatchers.Main) {
            segmentedButtonGroup.position = 0
            segmentBtn0.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_one)
            segmentedButtonGroup.setOnClickedButtonListener {
                when (it) {
                    0 -> showWeekData()
                    1 -> showMonthData()
                    2 -> showYearData()
                }
            }
        }
    }

    private fun showWeekData() {
        showCategoryAdapter("week")
        setUserInfo("week")
        pieData.getDataByTime(getWeekExpenses().second)
        pieData.createPieChart(pieChart)
        segmentBtn0.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_one)
        segmentBtn1.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_two_white)
        segmentBtn2.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_three_white)
    }

    private fun showMonthData() {
        showCategoryAdapter("month")
        setUserInfo("month")
        pieData.getDataByTime(getMonthExpenses().second)
        pieData.createPieChart(pieChart)
        segmentBtn1.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_two)
        segmentBtn0.background = ContextCompat.getDrawable(context!!, R.drawable.segmented_button_one_white)
        segmentBtn2.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_three_white)
    }

    private fun showYearData() {
        showCategoryAdapter("year")
        setUserInfo("year")
        pieData.getDataByTime(getYearExpenses().second)
        pieData.createPieChart(pieChart)
        segmentBtn2.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_three)
        segmentBtn0.background = ContextCompat.getDrawable(context!!, R.drawable.segmented_button_one_white)
        segmentBtn1.background = ContextCompat.getDrawable(context!!, R.drawable.segment_button_two_white)
    }

    private fun getTransactionsByTime() {
        try {
            GlobalScope.launch {
                getCurrentUser()
                getTransactionsByWeek()
                getTransactionsByMonth()
                getTransactionsByYear()
                setUserInfo("week")
                showCategoryAdapter("week")
                initButtonListener()
            }
        } catch (ex: Exception) {
            showMessage(getString(R.string.something_went_wrong))
        }
    }

    private suspend fun getCurrentUser() {
        user = GlobalScope.async(Dispatchers.IO) {
            SharedPrefUtil(context!!.applicationContext).getEmail()?.let {
                UserRepository(context!!.applicationContext).getPrimaryUser(it)
            }
        }.await()
    }

    private suspend fun getTransactionsByWeek() {
        GlobalScope.async(Dispatchers.IO) {
            val startWeekDate = DateUtil().getWeekStartDate()
            val endWeekDate = DateUtil().getWeekEndDate()
            user?.let {
                weekTransactionList = TransactionRepository(context!!.applicationContext)
                        .getTransactionByTime(it.id, startWeekDate, endWeekDate)
            }
        }.await()
    }

    private suspend fun getTransactionsByMonth() {
        GlobalScope.async(Dispatchers.IO) {
            val startMonthDate = DateUtil().getFirstDayOfMonth()
            val endMonthDate = DateUtil().getLastDayOfMonth()
            user?.let {
                monthTransactionList = TransactionRepository(context!!.applicationContext)
                        .getTransactionByTime(it.id, startMonthDate, endMonthDate)
            }
        }.await()
    }

    private suspend fun getTransactionsByYear() {
        GlobalScope.async(Dispatchers.IO) {
            val startYearDate = DateUtil().getFirstLastDayOfYear().first
            val endYearDate = DateUtil().getFirstLastDayOfYear().second
            user?.let {
                yearTransactionList = TransactionRepository(context!!.applicationContext)
                        .getTransactionByTime(it.id, startYearDate, endYearDate)
            }
        }.await()
    }

    private fun setUserInfo(textPeriod: String) {
        GlobalScope.async(Dispatchers.Main) {
            if (textPeriod == "week") {
                totalExpenses.text = String.format("%.2f", getWeekExpenses().first)
                amountLeft.text = String.format("%.2f", getCurrentIncomeWeekBalance().second)
                if (!getWeekExpenses().second.isEmpty()) {
                    pieData.getDataByTime(getWeekExpenses().second)
                }
                pieData.createPieChart(pieChart)
            }
            if (textPeriod == "month") {
                pieData.createPieChart(pieChart)
                totalExpenses.text = String.format("%.2f", getMonthExpenses().first)
                amountLeft.text = String.format("%.2f", getMonthIncomeAndBalance().second)
            }
            if (textPeriod == "year") {
                pieData.createPieChart(pieChart)
                totalExpenses.text = String.format("%.2f", getYearExpenses().first)
                amountLeft.text = String.format("%.2f", getCurrentYearBalance())
            }
        }
    }

    private fun showCategoryAdapter(adapterType: String) {
        GlobalScope.async(Dispatchers.Main) {
            when (adapterType) {
                "week" -> expensesAdaper = ExpensesAdapter(getWeekExpenses().second, getWeekExpensesAmountList())
                "month" -> expensesAdaper = ExpensesAdapter(getMonthExpenses().second, getMonthExpensesAmountList())
                "year" -> expensesAdaper = ExpensesAdapter(getYearExpenses().second, getYearExpensesAmountLists())
            }
            expensesRecyclerView.layoutManager = LinearLayoutManager(activity)
            expensesRecyclerView.adapter = expensesAdaper
            expensesAdaper!!.notifyDataSetChanged()
        }
    }

    private fun getWeekExpenses(): Pair<Double, MutableList<Transaction>> {
        val weekExpensiveList = mutableListOf<Transaction>()
        var weekExpensesAmount = 0.0
        for (transactions in weekTransactionList) {
            if (transactions.name != "Income") {
                weekExpensesAmount += transactions.amount
                weekExpensiveList.add(transactions)
            }
        }
        return Pair(weekExpensesAmount, weekExpensiveList)
    }

    private fun getMonthExpenses(): Pair<Double, MutableList<Transaction>> {
        val monthExpensiveList = mutableListOf<Transaction>()
        var monthExpenses = 0.0
        for (transactions in monthTransactionList) {
            if (transactions.name != "Income") {
                monthExpenses += transactions.amount
                monthExpensiveList.add(transactions)
            }
        }
        return Pair(monthExpenses, monthExpensiveList)
    }

    private fun getCurrentIncomeWeekBalance(): Pair<Double, Double> {
        val weekExpenses = getWeekExpenses().first
        var weekIncome = 0.0
        for (transactions in weekTransactionList) {
            if (transactions.name == "Income") {
                weekIncome += transactions.amount
            }
        }
        return Pair(weekIncome, weekIncome - weekExpenses)
    }

    private fun getYearExpenses(): Pair<Double, MutableList<Transaction>> {
        val yearExpensiveList = mutableListOf<Transaction>()
        var yearExpenses = 0.0
        for (transactions in yearTransactionList) {
            if (transactions.name != "Income") {
                yearExpenses += transactions.amount
                yearExpensiveList.add(transactions)
            }
        }
        return Pair(yearExpenses, yearExpensiveList)
    }

    private fun getCurrentYearBalance(): Double {
        val yearExpenses = getYearExpenses().first
        var yearIncome = 0.0
        for (transactions in yearTransactionList) {
            if (transactions.name == "Income") {
                yearIncome += transactions.amount
            }
        }
        return yearIncome - yearExpenses
    }

    private fun getMonthIncomeAndBalance(): Pair<Double, Double> {
        var monthIncomeAmount = 0.0
        var expenses = 0.0
        val currentMonthBalance: Double
        for (transactions in monthTransactionList) {
            if (transactions.name == "Income") {
                monthIncomeAmount += transactions.amount
            }
        }
        for (transactions in monthTransactionList) {
            if (transactions.name != "Income") {
                expenses += transactions.amount
            }
        }
        currentMonthBalance = monthIncomeAmount - expenses
        return Pair(monthIncomeAmount, currentMonthBalance)
    }

    private fun getWeekExpensesAmountList(): MutableList<Double> {
        val income = getCurrentIncomeWeekBalance().first
        val weekExpensiveList = getWeekExpensesList()
        val weekExpensesAmountList = mutableListOf<Double>()
        for (expenses in weekExpensiveList) {
            val amount = income - expenses
            weekExpensesAmountList.add(amount)
        }
        return weekExpensesAmountList
    }

    private fun getWeekExpensesList(): MutableList<Double> {
        val weekExpensiveList = mutableListOf<Double>()
        for (transactions in weekTransactionList) {
            if (transactions.name != "Income") {
                weekExpensiveList.add(transactions.amount)
            }
        }
        return weekExpensiveList
    }

    private fun getMonthExpensesAmountList(): MutableList<Double> {
        val income = getCurrentIncomeWeekBalance().first
        val monthExpensiveList = getMonthExpensesList()
        val monthExpensesAmountList = mutableListOf<Double>()
        for (expenses in monthExpensiveList) {
            val amount = income - expenses
            monthExpensesAmountList.add(amount)
        }
        return monthExpensesAmountList
    }

    private fun getMonthExpensesList(): MutableList<Double> {
        val monthExpensiveList = mutableListOf<Double>()
        for (transactions in monthTransactionList) {
            if (transactions.name != "Income") {
                monthExpensiveList.add(transactions.amount)
            }
        }
        return monthExpensiveList
    }

    private fun getYearExpensesAmountLists(): MutableList<Double> {
        val income = getCurrentIncomeWeekBalance().first
        val yearExpensiveList = getYearExpensesLists()
        val yearExpensesAmountsList = mutableListOf<Double>()
        for (expenses in yearExpensiveList) {
            val amount = income - expenses
            yearExpensesAmountsList.add(amount)
        }
        return yearExpensesAmountsList
    }

    private fun getYearExpensesLists(): MutableList<Double> {
        val yearExpensiveList = mutableListOf<Double>()
        for (transactions in yearTransactionList) {
            if (transactions.name != "Income") {
                yearExpensiveList.add(transactions.amount)
            }
        }
        return yearExpensiveList
    }

    private fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}

package com.assist.iuriegaitur.expensemanager.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    private val calendar = GregorianCalendar.getInstance(Locale.FRANCE)
    private val year = calendar.get(Calendar.YEAR)
    private val month = calendar.get(Calendar.MONTH)
    private val day = 1

    fun getWeekStartDate(): Long {
        calendar.add(Calendar.DAY_OF_WEEK, -(calendar.get(Calendar.DAY_OF_WEEK) - 1))
        return calendar.timeInMillis
    }

    fun getWeekEndDate(): Long {
        return if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.timeInMillis
        } else {
            val weekday = calendar.get(Calendar.DAY_OF_WEEK)
            var days = Calendar.SUNDAY - weekday
            if (days < 0) {
                days += 7
            }
            calendar.add(Calendar.DAY_OF_YEAR, days)
            calendar.timeInMillis
        }
    }

    fun getFirstDayOfMonth(): Long {
        calendar.set(year, month, day)
        calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        return calendar.timeInMillis
    }

    fun getLastDayOfMonth(): Long {
        calendar.add(Calendar.DAY_OF_MONTH, 1)
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
        return calendar.timeInMillis
    }

    fun getAllFirstMonthDayList(): MutableList<Long> {
        var month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        calendar.clear()
        calendar.set(Calendar.YEAR, year)
        val firstDayList = mutableListOf<Long>()

        while (month < 12) {
            month++
            calendar.add(Calendar.MONTH, -1)
            val firstDay = calendar.timeInMillis
            firstDayList.add(firstDay)
        }
        return firstDayList
    }

    fun getAllLastMonthDayList(): Pair<MutableList<Long>, MutableList<String>> {
        val lastDayList = mutableListOf<Long>()
        val monthsNameList= mutableListOf<String>()
        var month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        calendar.clear()
        calendar.set(Calendar.YEAR, year)
        while (month < 12) {
            month++
            calendar.add(Calendar.MONTH, -1)
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
            lastDayList.add(calendar.timeInMillis)
            monthsNameList.add(SimpleDateFormat("MMM").format(calendar.time).toString())
        }
        return Pair(lastDayList,monthsNameList)
    }

    fun getFirstLastDayOfYear(): Pair<Long, Long> {
        calendar.set(Calendar.DAY_OF_YEAR, 1)
        val yearStartDate = calendar.timeInMillis

        calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR))
        val yearEndDate = calendar.timeInMillis
        return Pair(yearStartDate, yearEndDate)
    }

    fun getCurrentDate(): String {
        val currentDate = calendar.time
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH)
        return dateFormat.format(currentDate)
    }

    fun getCurrentHour(): String {
        val currentTime = Calendar.getInstance().time
        val timeFormat = SimpleDateFormat("HH:mm", Locale.FRENCH)
        return timeFormat.format(currentTime).toString()
    }
}
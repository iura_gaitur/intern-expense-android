package com.assist.iuriegaitur.expensemanager.utils

import com.assist.iuriegaitur.expensemanager.R
import java.util.regex.Pattern

class ValidateUtil {
    private val specialCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
    private val UpperCasePatten = Pattern.compile("[A-Z ]")
    private val lowerCasePatten = Pattern.compile("[a-z ]")
    private val digitCasePatten = Pattern.compile("[0-9 ]")

    fun validateUsername(username: String): Int? {
        if (username.isEmpty()) {
            return R.string.enter_username
        }
        if (digitCasePatten.matcher(username).find()) {
            return R.string.only_characters_username

        }
        if (specialCharPatten.matcher(username).find()) {
            return R.string.only_characters_username
        }
        return null
    }

    fun validateEmail(email: String): Int? {
        if (email.isEmpty()) {
            return R.string.enter_email
        }
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
        } else {
            return R.string.invalid_email
        }
        return null
    }

    fun validatePassword(password: String): Int? {
        if (password.isEmpty()) {
            return R.string.enter_pass
        }

        if (password.length > 20) {
            return R.string.min_characters_pass
        }
        if (password.length < 5) {
            return R.string.min_length_pass
        }
        if (!digitCasePatten.matcher(password).find()
                && !specialCharPatten.matcher(password).find()
                && !UpperCasePatten.matcher(password).find()) {
            return R.string.error_general_pass

        }
        if (!specialCharPatten.matcher(password).find()) {
            return R.string.special_character_pass
        }
        if (!UpperCasePatten.matcher(password).find()) {
            return R.string.special_character_pass
        }
        if (!lowerCasePatten.matcher(password).find()) {
            return R.string.lowercase_pass
        }
        if (!digitCasePatten.matcher(password).find()) {
            return R.string.one_digit_pass
        }
        return null
    }
}
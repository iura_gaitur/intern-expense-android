package com.assist.iuriegaitur.expensemanager.utils

import android.graphics.Color
import com.assist.iuriegaitur.expensemanager.models.Transaction
import com.assist.iuriegaitur.expensemanager.utils.ChartHelper.entries
import com.assist.iuriegaitur.expensemanager.utils.ChartHelper.entriesBar
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.ViewPortHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.ArrayList

class ChartUtil {
    private val data = CombinedData()
    private val count = 12
    private val months = DateUtil().getAllLastMonthDayList().second
    private lateinit var xAxis: XAxis
    private var food = 0.0f
    private var car = 0.0f
    private var clothes = 0.0f
    private var savings = 0.0f
    private var health = 0.0f
    private var beauty = 0.0f
    private var travel = 0.0f
    private var budget = 0.0f

    fun createCombineChart(chart: CombinedChart) {
        chart.description.isEnabled = false
        chart.setBackgroundColor(Color.WHITE)
        chart.setDrawGridBackground(false)
        chart.setDrawBarShadow(false)
        chart.isHighlightFullBarEnabled = false
        chart.drawOrder = arrayOf(CombinedChart.DrawOrder.BAR, CombinedChart.DrawOrder.LINE)

        generateCombinedLegend(chart)
        generateCombinedXaxes(chart)
        data.setData(generateLineData())
        data.setData(generateBarData())

        xAxis.axisMaximum = data.xMax + 0.19f
        chart.data = data
        chart.animateXY(2000, 2000)
        chart.invalidate()
    }

    private fun generateCombinedXaxes(chart: CombinedChart) {
        val rightAxis = chart.axisRight
        rightAxis.setDrawGridLines(false)
        rightAxis.axisMinimum = 0f

        val leftAxis = chart.axisLeft
        leftAxis.setDrawGridLines(false)
        leftAxis.axisMinimum = 0f

        xAxis = chart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTH_SIDED
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.valueFormatter = object : IValueFormatter, IAxisValueFormatter {
            override fun getFormattedValue(value: Float, axis: AxisBase?): String {
                return months[value.toInt() % months.size]
            }

            override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex: Int,
                                           viewPortHandler: ViewPortHandler?): String {
                return months[value.toInt() % months.size]
            }
        }
    }

    private fun generateCombinedLegend(chart: CombinedChart) {
        val legend = chart.legend
        legend.isWordWrapEnabled = true
        legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        legend.orientation = Legend.LegendOrientation.HORIZONTAL
        legend.setDrawInside(false)
    }

    fun addMonthData(monthDataList: MutableList<Transaction>, index: Float) {
        budget = 0.0f
        var incomeMonth = 0.0f
        var expensesMonth = 0.0f


        for (income in monthDataList) {
            if (income.name == "Income") {
                incomeMonth += income.amount.toFloat()
            }
        }

        for (expenses in monthDataList) {
            if (expenses.name != "Income") {
                expensesMonth += expenses.amount.toFloat()
            }
        }

        budget = incomeMonth - expensesMonth
        GlobalScope.launch {
            if (budget == 0.0f) {
                createMonthData(index, 0.0f)
            } else {
                createMonthData(index, budget)
            }
        }
    }

    private fun createMonthData(index: Float, buget: Float) {
        entries.add(Entry(index, budget))
        entriesBar.add(BarEntry(0f, buget))

        if (entries.isEmpty()) {
            for (index in 0 until count) {
                entries.add(Entry(index + 0.8f, 1f))
                entriesBar.add(BarEntry(0f, 1f))
            }
        }
    }

    private fun generateLineData(): LineData {
        val lineData = LineData()
        val set = LineDataSet(entries, "Line DataSet")
        set.color = Color.rgb(240, 238, 70)
        set.lineWidth = 2.5f
        set.setCircleColor(Color.rgb(240, 238, 70))
        set.circleRadius = 5f
        set.fillColor = Color.rgb(240, 238, 70)
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.setDrawValues(true)
        set.valueTextSize = 10f
        set.valueTextColor = Color.rgb(240, 238, 70)
        set.axisDependency = YAxis.AxisDependency.LEFT
        lineData.addDataSet(set)

        return lineData
    }

    private fun generateBarData(): BarData {
        val entries2 = ArrayList<BarEntry>()
        val groupSpace = 0.00f
        val barSpace = 0.00f
        val barWidth = 0.4f
        val set1 = BarDataSet(entriesBar, "Bar 1")
        val set2 = BarDataSet(entries2, "")
        val barData = BarData(set1, set2)

        set1.color = Color.rgb(60, 220, 78)
        set1.valueTextColor = Color.rgb(60, 220, 78)
        set1.valueTextSize = 10f
        set1.axisDependency = YAxis.AxisDependency.LEFT
        barData.barWidth = barWidth
        barData.groupBars(0f, groupSpace, barSpace)
        return barData
    }

    fun createPieChart(chart: PieChart) {
        customiseChart(chart)
        createPieLegend(chart)
        generatePieChartData(chart)
    }

    private fun customiseChart(chart: PieChart) {
        chart.centerText = "My Expenses"
        chart.isDrawHoleEnabled = true
        chart.setHoleColor(Color.WHITE)
        chart.setTransparentCircleColor(Color.WHITE)
        chart.setTransparentCircleAlpha(110)
        chart.holeRadius = 58f
        chart.transparentCircleRadius = 61f
        chart.setDrawCenterText(true)
        chart.rotationAngle = 0f
        chart.isRotationEnabled = true
        chart.isHighlightPerTapEnabled = true
        chart.animateY(1400, Easing.EaseInOutQuad)
        chart.setEntryLabelColor(Color.WHITE)
        chart.setEntryLabelTextSize(12f)
    }

    private fun createPieLegend(chart: PieChart) {
        val l = chart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f
    }

    private fun generatePieChartData(chart: PieChart) {
        val entries = ArrayList<PieEntry>()
        if (food != 0.0f) entries.add(PieEntry(food, "Food"))
        if (car != 0.0f) entries.add(PieEntry(car, "Car"))
        if (clothes != 0.0f) entries.add(PieEntry(clothes, "Clothes"))
        if (savings != 0.0f) entries.add(PieEntry(savings, "Savings"))
        if (health != 0.0f) entries.add(PieEntry(health, "Health"))
        if (beauty != 0.0f) entries.add(PieEntry(beauty, "Beauty"))
        if (travel != 0.0f) entries.add(PieEntry(travel, "Travel"))

        if (entries.isEmpty()) {
            entries.add(PieEntry(1f, "Expenses"))
            entries.add(PieEntry(1f, "Expenses"))
            entries.add(PieEntry(1f, "Expenses"))
        }
        setDataChart(entries, chart)
    }

    private fun setDataChart(entriesList: MutableList<PieEntry>, chart: PieChart) {
        val dataSet = PieDataSet(entriesList, "Expenses")

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        setColorsPieChart(dataSet)

        val data = PieData(dataSet)
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        chart.data = data
        chart.highlightValues(null)
        chart.invalidate()
    }

    private fun setColorsPieChart(dataSet: PieDataSet) {
        val colorsList = ArrayList<Int>()
        val colorTemplate = ColorTemplate.MATERIAL_COLORS + ColorTemplate.JOYFUL_COLORS +
                ColorTemplate.COLORFUL_COLORS + ColorTemplate.LIBERTY_COLORS + ColorTemplate.PASTEL_COLORS
        for (color in colorTemplate) {
            colorsList.add(color)
        }
        colorsList.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colorsList
    }

    fun getDataByTime(listTransactions: MutableList<Transaction>) {

        for (transaction in listTransactions) {
            when (transaction.name) {
                "Food" -> food = food.plus(transaction.amount.toFloat())
                "Car" -> car = car.plus(transaction.amount.toFloat())
                "Clothes" -> clothes = clothes.plus(transaction.amount.toFloat())
                "Savings" -> savings = savings.plus(transaction.amount.toFloat())
                "Health" -> health = health.plus(transaction.amount.toFloat())
                "Beauty" -> beauty = beauty.plus(transaction.amount.toFloat())
                "Travel" -> travel = travel.plus(transaction.amount.toFloat())
            }
        }
    }
}


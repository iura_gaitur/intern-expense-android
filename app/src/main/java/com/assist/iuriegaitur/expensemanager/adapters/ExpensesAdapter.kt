package com.assist.iuriegaitur.expensemanager.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.R.drawable.*
import com.assist.iuriegaitur.expensemanager.models.Transaction
import kotlinx.android.synthetic.main.row_expenses.view.*
import java.text.SimpleDateFormat
import java.util.*

class ExpensesAdapter(private var transactionList: MutableList<Transaction>, private var expensiveList: MutableList<Double>)
    : RecyclerView.Adapter<ExpensesAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_expenses, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.onBindView(transactionList[position], expensiveList[position])
    }

    override fun getItemCount(): Int = transactionList.size

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBindView(transaction: Transaction, expensiveList: Double) = with(itemView) {
            when (transaction.name) {
                "Income" -> imageCardView.setImageDrawable(context.getDrawable(ic_hand))
                "Food" -> imageCardView.setImageDrawable(context.getDrawable(ic_groceries))
                "Car" -> imageCardView.setImageDrawable(context.getDrawable(ic_car))
                "Clothes" -> imageCardView.setImageDrawable(context.getDrawable(ic_laundry))
                "Savings" -> imageCardView.setImageDrawable(context.getDrawable(ic_savings))
                "Health" -> imageCardView.setImageDrawable(context.getDrawable(ic_doctor))
                "Beauty" -> imageCardView.setImageDrawable(context.getDrawable(ic_beauty))
                "Travel" -> imageCardView.setImageDrawable(context.getDrawable(ic_travel))
            }
            amount.text = String.format("%.2f", expensiveList)
            categoryExpenses.text = transaction.name
            expense.text = String.format("%.2f", transaction.amount)
            val dateFormat = SimpleDateFormat("EEEE", Locale.ENGLISH)
            val formattedDate = dateFormat.format(transaction.dateTime)
            date.text = formattedDate
        }
    }
}
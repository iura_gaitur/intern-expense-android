package com.assist.iuriegaitur.expensemanager.utils

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.support.v4.app.DialogFragment
import android.widget.TimePicker
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("ValidFragment")
class TimePickerUtil(private val alertDialogInterface: TimeAlertDialogInterface) :
        DialogFragment(), TimePickerDialog.OnTimeSetListener {

    private lateinit var calendar: Calendar

    fun showPicker(view: View) {
        calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        TimePickerDialog(view.context, this, hour, minute, true).show()
    }

    override fun onTimeSet(view: TimePicker, hour: Int, minute: Int) {
        calendar.set(0, 0, 0, hour, minute, 0)
        val time = calendar.time
        val dateFormat = SimpleDateFormat("HH:mm", Locale.FRENCH)
        val formattedDate = dateFormat.format(time)
        alertDialogInterface.timeInput(formattedDate)
        alertDialogInterface.timeInputWithoutFormat(hour, minute, 0)
    }

    interface TimeAlertDialogInterface {
        fun timeInput(time: String)
        fun timeInputWithoutFormat(hour: Int, minute: Int, seconds: Int)
    }
}
package com.assist.iuriegaitur.expensemanager.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import com.assist.iuriegaitur.expensemanager.utils.ValidateUtil
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.coroutines.*
import java.lang.Exception

class RegisterActivity : AppCompatActivity() {

    private lateinit var username: String
    private lateinit var password: String
    private lateinit var email: String
    private var id: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initializeActionListeners()
    }

    private fun initializeActionListeners() {
        login.setOnClickListener { showLoginActivity() }
        registerButton.setOnClickListener { registerUser() }
    }

    private fun registerUser() {
        if (!validateFormInputs()) return

        try {
            GlobalScope.launch { saveUser() }
            saveUserSharedPreferences()
            showMainActivity()
        } catch (ex: Exception) {
            showMessage("Something went wrong on saving the user")
        }
    }

    private suspend fun saveUser() {
        GlobalScope.async(Dispatchers.IO) {
            id = System.currentTimeMillis()
            UserRepository(this@RegisterActivity).savePrimaryUser(User(name = username, email = email, id = id))
        }.await()
    }

    private fun saveUserSharedPreferences() {
        SharedPrefUtil(this).saveUserSharedPreferences(email, id)
    }

    private fun showMessage(message: String) {
        Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun showMainActivity() {
        MainActivity.show(this)
        finish()
    }

    private fun showLoginActivity() {
        LoginActivity.show(this)
        finish()
    }

    private fun validateFormInputs(): Boolean {
        username = activationUsernameField.text.toString().trim()
        email = activationEmailField.text.toString().trim()
        password = activationPasswordField.text.toString().trim()

        val validateEmailError = ValidateUtil().validateEmail(email)
        val validatePasswordError = ValidateUtil().validatePassword(password)
        val validateUsernameError = ValidateUtil().validateUsername(username)

        if (validateUsernameError != null) {
            activationUsernameField.error = this.getString(validateUsernameError)
            return false
        }
        if (validateEmailError != null) {
            activationEmailField.error = this.getString(validateEmailError)
            return false
        }
        if (validatePasswordError != null) {
            activationPasswordField.error = this.getString(validatePasswordError)
            return false
        }
        return true
    }

    companion object {
        fun show(activity: Activity) {
            activity.startActivity(Intent(activity, RegisterActivity::class.java))
        }
    }
}

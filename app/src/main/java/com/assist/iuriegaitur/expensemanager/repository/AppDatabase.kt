package com.assist.iuriegaitur.expensemanager.repository

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.assist.iuriegaitur.expensemanager.models.Transaction
import com.assist.iuriegaitur.expensemanager.models.User

@Database(entities = [User::class, Transaction::class], version = 18, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun transactionDao(): TransactionDao
}
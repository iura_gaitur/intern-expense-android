package com.assist.iuriegaitur.expensemanager.repository

import android.content.Context
import com.assist.iuriegaitur.expensemanager.models.User

class UserRepository(private val context: Context) {
    fun getPrimaryUser(email: String): User? {
        return RoomDatabase(context).appDatabase.userDao().getPrimaryUser(email)
    }

    fun savePrimaryUser(user: User) {
        return RoomDatabase(context).appDatabase.userDao().savePrimaryUser(user)
    }

    fun checkIfUserExist(email: String): Boolean {
        return RoomDatabase(context).appDatabase.userDao().checkIfUserExist(email)
    }
}

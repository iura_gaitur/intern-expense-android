package com.assist.iuriegaitur.expensemanager.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        showNextScreen()
    }

    private fun showNextScreen() {
        GlobalScope.launch {
            if (checkUserIsLoggedIn()) {
                showMainScreen()
            } else {
                showLoginScreen()
            }
        }
    }

    private suspend fun checkUserIsLoggedIn(): Boolean {
        var userExist = false
        GlobalScope.async(IO) {
            delay(2000)
            SharedPrefUtil(applicationContext).getEmail()?.let { userExist = it.isNotEmpty() }
        }.await()
        return userExist
    }

    private fun showLoginScreen() {
        LoginActivity.show(this)
    }

    private fun showMainScreen() {
        MainActivity.show(this)
    }
}

package com.assist.iuriegaitur.expensemanager.utils

import android.content.Context

class SharedPrefUtil(val context: Context) {
    private val prefs = context.getSharedPreferences("MY_PREF_NAME", Context.MODE_PRIVATE)
    private val editor = prefs.edit()

    fun saveUserSharedPreferences(email: String, id: Long?) {
        editor.putString("LOGIN_EMAIL", email)
        id?.let { editor.putLong("LOGIN_ID", it) }
        editor.apply()
    }

    fun clearSharedPreferences() {
        editor.clear()
        editor.apply()
    }

    fun getEmail(): String? {
        return prefs.getString("LOGIN_EMAIL", "")
    }
}
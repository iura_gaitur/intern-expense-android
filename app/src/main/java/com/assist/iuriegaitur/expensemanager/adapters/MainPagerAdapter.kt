package com.assist.iuriegaitur.expensemanager.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.models.Category
import kotlinx.android.synthetic.main.row_category.view.*

class MainPagerAdapter(private var categoryList: MutableList<Category>,
                       private val onUserAction: MainPagerAdapter.OnUserAction)
    : RecyclerView.Adapter<MainPagerAdapter.EventViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_category, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.onBindView(categoryList[position], onUserAction)
    }

    override fun getItemCount(): Int = categoryList.size

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBindView(category: Category, onUserAction: MainPagerAdapter.OnUserAction) = with(itemView) {
            imageCardView.setImageDrawable(context.getDrawable(category.image))
            categoryTitle.text = category.title
            itemView.setOnClickListener { onUserAction.onUserClickShowCategory(category) }

            if (category.categoryOnClick) {
                card_view.background = ContextCompat.getDrawable(context, R.drawable.shape_card_orange)
            } else {
                card_view.background = ContextCompat.getDrawable(context, R.drawable.shape_card_green)
            }
        }
    }

    fun stateCategory(id: Int) {
        for (category in categoryList) {
            category.categoryOnClick = false
        }
        categoryList.first { it.id == id }.categoryOnClick = true
        notifyDataSetChanged()
    }

    interface OnUserAction {
        fun onUserClickShowCategory(category: Category)
    }
}

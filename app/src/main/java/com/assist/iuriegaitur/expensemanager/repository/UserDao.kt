package com.assist.iuriegaitur.expensemanager.repository

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.assist.iuriegaitur.expensemanager.models.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE email = :email")
    fun getPrimaryUser(email: String): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePrimaryUser(user: User)

    @Query("SELECT * FROM user WHERE email = :email")
    fun checkIfUserExist(email: String): Boolean
}
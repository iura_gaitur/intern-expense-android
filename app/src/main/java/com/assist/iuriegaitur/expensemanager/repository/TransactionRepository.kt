package com.assist.iuriegaitur.expensemanager.repository

import android.content.Context
import com.assist.iuriegaitur.expensemanager.models.Transaction

class TransactionRepository(private val context: Context) {

    fun getTransaction(userId: Long): MutableList<Transaction> {
        return RoomDatabase(context).appDatabase.transactionDao().getTransaction(userId)
    }

    fun saveTransaction(transaction: Transaction) {
        return RoomDatabase(context).appDatabase.transactionDao().saveTransaction(transaction)
    }

    fun getTransactionByTime(userId: Long, start: Long, endDate: Long): MutableList<Transaction> {
        return RoomDatabase(context).appDatabase.transactionDao().getTransactionFromSpecificTime(userId, start, endDate)
    }
}
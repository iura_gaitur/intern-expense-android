package com.assist.iuriegaitur.expensemanager.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "user")
class User {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "email")
    var email: String? = null

    @Ignore
    constructor()

    constructor(id: Long = 0, name: String? = null, email: String? = null) {
        this.id = id
        this.name = name
        this.email=email
    }
}

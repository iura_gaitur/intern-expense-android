package com.assist.iuriegaitur.expensemanager.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.network.ResponseCurrency
import com.google.gson.Gson
import com.squareup.okhttp.*
import kotlinx.android.synthetic.main.activity_converter.*
import java.io.IOException
import com.squareup.okhttp.HttpUrl
import org.json.JSONException
import android.widget.ArrayAdapter
import com.assist.iuriegaitur.expensemanager.network.convertor
import kotlin.collections.ArrayList
import com.assist.iuriegaitur.expensemanager.network.CurrencyModel
import com.assist.iuriegaitur.expensemanager.utils.DateUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class ConverterActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val client = OkHttpClient()
    private val gson = Gson()
    private lateinit var currencies: ResponseCurrency
    private var spinnerPosition = 0
    private var rate = CurrencyModel()
    private var foreignAmount = ""
    private var ronAmount = ""
    private var ronWatcher: TextWatcher? = null
    private var foreignWatcher: TextWatcher? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_converter)

        getCurrencies()
        initActionListeners()
        setConvertingDate()
    }

    private fun getCurrencies() {
        val request = initHttpRequest()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(request: Request?, e: IOException?) {}

            override fun onResponse(response: Response) {
                if (response.code() == 200) {
                    val myResponse = response.body().string()
                    try {
                        currencies = gson.fromJson(myResponse, ResponseCurrency::class.java)
                        GlobalScope.launch { setSpinnerAdapter() }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    private fun initHttpRequest(): Request? {
        val urlBuilder = HttpUrl.parse("https://romanian-exchange-rate-bnr-api.herokuapp.com/api/latest?").newBuilder()
        urlBuilder.addQueryParameter("access_key", "f7dbe1842278-43779b")
        val url = urlBuilder.build().toString()

        return Request.Builder().url(url).build()
    }

    private fun initActionListeners() {
        arrowBack.setOnClickListener {
            finish()
        }
    }

    private fun setConvertingDate() {
        val date = DateUtil().getCurrentDate()
        convertingText.text = "Converted  using BNR at date $date"
    }

    private suspend fun setSpinnerAdapter() {
        GlobalScope.async(Dispatchers.Main) {
            var currencyNamesList = ArrayList<String>()
            for (currencyNames in convertor(currencies.rates)) {
                currencyNamesList.add(currencyNames.currencyName)
            }

            val spinnerArrayAdapter = ArrayAdapter<String>(
                    this@ConverterActivity, android.R.layout.simple_spinner_item, currencyNamesList)
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerCurrency.adapter = spinnerArrayAdapter
            spinnerCurrency.onItemSelectedListener = this@ConverterActivity
        }.await()
    }

    private fun convertAmount() {
        if (!foreignAmount.isNullOrEmpty()) {
            foreignAmount = foreign.text.toString()
            convertRon(foreignAmount)
        }

        if (!ronAmount.isNullOrEmpty()) {
            ronAmount = ron.text.toString()
            convertForeign(ronAmount)
        }
        checkOntapListener()
    }

    private fun checkOntapListener() {
        foreign.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus)
                initForeignEditTextListener()
        }

        ron.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus)
                initRonEditTextListener()
        }
    }

    private fun initForeignEditTextListener() {
        ronWatcher?.let {
            ron.removeTextChangedListener(it)
            ronWatcher = null
        }

        foreignWatcher = object : TextWatcher {
            override fun afterTextChanged(text: Editable) {
                if (text.isNotEmpty()) {
                    foreignAmount = text.toString()
                    convertRon(text.toString())
                } else {
                    clearValues()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        }

        foreign.addTextChangedListener(foreignWatcher)
    }

    private fun initRonEditTextListener() {
        foreignWatcher?.let {
            foreign.removeTextChangedListener(it)
            foreignWatcher = null
        }

        ronWatcher = object : TextWatcher {
            override fun afterTextChanged(text: Editable) {

                if (text.isNotEmpty()) {
                    ronAmount = text.toString()
                    convertForeign(text.toString())
                } else {
                    clearValues()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        }

        ron.addTextChangedListener(ronWatcher)
    }

    private fun convertRon(foreignAmount: String) {
        if (!foreignAmount.isNullOrEmpty()) {
            val exchangeRate = rate.currencyRate
            val foreignTotal = foreignAmount.toDouble()
            val result = exchangeRate * foreignTotal

            ronWatcher?.let {
                ron.removeTextChangedListener(it)
                ronWatcher = null
            }
            ron.text = SpannableStringBuilder(String.format("%.2f", result))
        }
    }

    private fun clearValues() {
        ron.text.clear()
        foreign.text.clear()
    }

    private fun convertForeign(ronAmount: String) {
        if (!ronAmount.isNullOrEmpty()) {
            val exchangeRate = rate.currencyRate
            val ronTotal = ronAmount.toDouble()
            val result = ronTotal / exchangeRate
            foreignWatcher?.let {
                foreign.removeTextChangedListener(it)
                foreignWatcher = null
            }
            foreign.text = SpannableStringBuilder(String.format("%.2f", result))
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        spinnerPosition = position
        rate = convertor(currencies.rates)[spinnerPosition]
        convertAmount()
        setFlagIncon(rate)
    }

    private fun setFlagIncon(rate: CurrencyModel) {
        when (rate.currencyName) {
            "USD" -> foreignFlag.background = ContextCompat.getDrawable(this, R.drawable.ic_united_states)
            "EUR" -> foreignFlag.background = ContextCompat.getDrawable(this, R.drawable.ic_euro)
            "GBP" -> foreignFlag.background = ContextCompat.getDrawable(this, R.drawable.ic_united_kingdom)
            "PLN" -> foreignFlag.background = ContextCompat.getDrawable(this, R.drawable.ic_poland)
            "RUB" -> foreignFlag.background = ContextCompat.getDrawable(this, R.drawable.ic_russia)
        }
    }

    companion object {
        fun show(activity: Activity) {
            activity.startActivity(Intent(activity, ConverterActivity::class.java))
        }
    }
}
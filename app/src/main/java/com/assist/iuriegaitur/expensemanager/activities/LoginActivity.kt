package com.assist.iuriegaitur.expensemanager.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import com.assist.iuriegaitur.expensemanager.utils.ValidateUtil
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.*
import java.lang.Exception

class LoginActivity : AppCompatActivity() {

    private lateinit var password: String
    private lateinit var email: String
    var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initializeActionListeners()
    }

    private fun initializeActionListeners() {
        register.setOnClickListener { RegisterActivity.show(this) }
        activationLoginButton.setOnClickListener { loginUser() }
    }

    private fun loginUser() {
        if (!validateFormInputs()) return

        try {
            GlobalScope.launch {
                if (UserRepository(this@LoginActivity).checkIfUserExist(email)) {
                    getUser()
                    saveUserSharedPreferences()
                    showMainActivity()
                } else {
                    withContext(Dispatchers.Main) {
                        showMessage(getString(R.string.user_no_exists))
                    }
                }
            }
        } catch (ex: Exception) {
            showMessage(getString(R.string.went_wrong_login))
        }
    }

    private fun validateFormInputs(): Boolean {
        email = loginEmailField.text.toString().trim()
        password = loginPasswordField.text.toString().trim()

        val validateEmailError = ValidateUtil().validateEmail(email)
        val validatePasswordError = ValidateUtil().validatePassword(password)

        if (validateEmailError != null) {
            loginEmailField.error = this.getString(validateEmailError)
            return false
        }
        if (validatePasswordError != null) {
            loginPasswordField.error = this.getString(validatePasswordError)
            return false
        }
        return true
    }

    private suspend fun getUser() {
        user = GlobalScope.async(Dispatchers.IO) {
            UserRepository(this@LoginActivity).getPrimaryUser(email)
        }.await()
    }

    private fun saveUserSharedPreferences() {
        user?.let {
            SharedPrefUtil(this).saveUserSharedPreferences(email, it.id)
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun showMainActivity() {
        MainActivity.show(this)
        finish()
    }

    companion object {
        fun show(activity: Activity) {
            activity.startActivity(Intent(activity, LoginActivity::class.java))
        }
    }
}


package com.assist.iuriegaitur.expensemanager.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import kotlinx.android.synthetic.main.activity_main.*
import android.view.MenuItem
import android.widget.TextView
import com.assist.iuriegaitur.expensemanager.R
import com.assist.iuriegaitur.expensemanager.fragments.BudgetFragment
import com.assist.iuriegaitur.expensemanager.fragments.ExpensesFragment
import com.assist.iuriegaitur.expensemanager.models.User
import com.assist.iuriegaitur.expensemanager.repository.UserRepository
import com.assist.iuriegaitur.expensemanager.utils.SharedPrefUtil
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    private var user: User? = null
    private var fragment = Fragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        initToolbar(getString(R.string.my_budget))
        initToolbarActions()
        addTabsChangeListener()
        getUserRepository()
        initMenuActions()
        showBudgetFragment()
    }

    private fun initDrawerMenu() {
        GlobalScope.async(Dispatchers.Main) {
            val inflater = drawerMenu.inflateHeaderView(R.layout.view_header_drawer)
            val tv = inflater.findViewById(R.id.userNameMenu) as TextView
            user?.let {
                val name = it.name.toString()
                tv.text = "Hello $name"
            }
        }
    }

    private fun getUserRepository() {
        GlobalScope.launch {
            getUser()
            initDrawerMenu()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RESPONSE_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getBooleanExtra(GET_RESULT, false)
                if (result) {
                    showBudgetFragment()
                }
            }
        }
    }

    private suspend fun getUser() {
        user = GlobalScope.async(Dispatchers.Default) {
            SharedPrefUtil(applicationContext).getEmail()?.let {
                UserRepository(this@MainActivity).getPrimaryUser(it)
            }
        }.await()
    }

    private fun initToolbar(title: String) {
        toolbar.title = title
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        supportActionBar?.setLogo(getDrawable(R.drawable.ic_menu_black_24dp))
        toolbar.titleMarginStart = (resources.displayMetrics.density * resources.getDimension(R.dimen.size_20)).toInt()
    }

    private fun initToolbarActions() {
        toolbar.getChildAt(1).setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun initMenuActions() {
        drawerMenu.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.main_page_item -> this.showMainScreen()
                R.id.converter_item -> this.showConverterScreen()
                else -> {
                    true
                }
            }
        }
        logout.setOnClickListener { logout() }
    }

    private fun showBudgetFragment() {
        fragment = BudgetFragment()
        loadFragment(fragment)
    }

    private fun showMainScreen(): Boolean {
        val fragment: Fragment = BudgetFragment()
        loadFragment(fragment)
        return true
    }

    private fun showConverterScreen(): Boolean {
        ConverterActivity.show(this)
        return true
    }

    private fun logout() {
        SharedPrefUtil(this).clearSharedPreferences()
        finish()
        LoginActivity.show(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add -> showAddActionScreen()
        }
        return true
    }

    private fun showAddActionScreen() {
        val intent = Intent(this, AddActionActivity::class.java)
        startActivityForResult(intent, RESPONSE_CODE)
    }

    private fun addTabsChangeListener() {
        bottomMenu.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_budget -> {
                    toolbar.title = getString(R.string.my_budget)
                    fragment = BudgetFragment()
                    loadFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.action_expense -> {
                    toolbar.title = getString(R.string.my_expenses)
                    fragment = ExpensesFragment()
                    loadFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    companion object {
        fun show(activity: Activity) {
            activity.startActivity(Intent(activity, MainActivity::class.java))
        }

        const val RESPONSE_CODE = 111
        const val GET_RESULT = "intent get extra result"
    }
}

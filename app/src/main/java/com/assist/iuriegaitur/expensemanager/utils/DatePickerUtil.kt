package com.assist.iuriegaitur.expensemanager.utils

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import java.util.*
import android.support.v4.app.DialogFragment
import java.text.SimpleDateFormat

@SuppressLint("ValidFragment")
class DatePickerUtil(private val alertDialogInterface: AlertDialogInterface) :
        DialogFragment(), DatePickerDialog.OnDateSetListener, TimePickerUtil.TimeAlertDialogInterface {
    var dateTime = Calendar.getInstance()
    private lateinit var calendar: Calendar

    override fun timeInput(time: String) {}

    override fun timeInputWithoutFormat(hour: Int, minute: Int, seconds: Int) {
        dateTime.set(Calendar.HOUR, hour)
        dateTime.set(Calendar.MINUTE, minute)
        dateTime.set(Calendar.SECOND, seconds)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        calendar.add(Calendar.DAY_OF_MONTH, 1)

        return DatePickerDialog(activity, 0, this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH)
        dateTime.set(Calendar.DAY_OF_MONTH, day)
        dateTime.set(Calendar.MONTH, month)
        dateTime.set(Calendar.YEAR, year)
        alertDialogInterface.setDate(dateFormat.format(dateTime.time))
        alertDialogInterface.setDateTime(dateTime.timeInMillis)
    }

    interface AlertDialogInterface {
        fun setDateTime(dateTime: Long)
        fun setDate(date: String)
    }
}
